# Patch Notes

## Compatibility
- Ready for v12 version.


## v1.2.3 10/19/2024
- Official update 1.2.3

## v1.2.2 08/25/2024
- Official update 1.2.2

## v1.2.1 08/24/2024
- Official update 1.2.1

## v1.2.0 08/14/2024
- Official update 1.2.0

## v1.1.1 07/31/2024
- Official update 1.1.1

## v1.0.8
- Official update 1.0.8

## v1.0.6
- initial version