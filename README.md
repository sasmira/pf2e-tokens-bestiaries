The Pathfinder Token Pack: Bestiaries module for Foundry Virtual Tabletop was created and produced by a small but dedicated group of Foundry Virtual Tabletop staff and contractors hand-selected from the Pathfinder community, with assistance from Paizo Inc.

The Token Ring provided within this pack was custom-created by Caeora for exclusive use in this project! If you enjoy it, please consider supporting Caeora via his patreon at [https://patreon.com/caeora] or his numerous other projects at [https://www.caeora.com/]
## Token Creation

Ian Hildebrandt

## Project Management

Viviane Charlier

Shane Martland

## Art Acquisition

Andrew White

## Announcement Trailer

Matt Ryan

## Additional Art Sources

The following creatures did not have official sources of art available at Paizo Inc. or had sources which required heavy modification, and were composited from existing art assets provided by Paizo Inc. with great care to respect the original artists' work. 

- Black Scorpion
- Chimera (White, Green, Black, Blue)
- Dracolisk (White, Red, Black, Blue)
- Eagle
- Empress Lava Worm
- Empress Worm Swarm (Lava, Ice, Necral, Mage-eater)
- Ghost Commoner
- Kobold Dragon Mage (Black, Red, Green, White)
- Living Graffiti (Blood, Chalk, Ink)
- Living Rune (Divine, Arcane, Primal)
- Petitioner (Abbadon, Axis, Boneyard, Elysium, Heaven, Hell, Maelstrom, Astral, Dead Vault, Ethereal, Material, Shadow, Dimension of Dreams, Air, Earth, Fire, Water, Positive Energy)
- Sinspawn (Lust)
- Skeleton Infantry
- Tera-cotta Garrison 
- Wizard Sponge (Crypt, Underwater, Fey, Fiendish, Toxic)

## Special Thanks

The Foundry Virtual Tabletop team would like to express its profound and heartfelt thanks to the development team of the PF2e game system. Their efforts to adapt Pathfinder Second Edition for virtual tabletop play have provided countless fans a place to play and experience Pathfinder at a level of quality unmatched anywhere else. Their adamant refusal to accept payment for their work on the system is a bold statement about the generosity of the Pathfinder community, and we look forward to working together with them for a long time to come.

**Development Leads**

Carlos Fernandez

Kevin Hjelden

Richard Simões

**Data Entry Leads**

Ian Blackstone

Ian Hildebrandt

**Community Manager**

Viviane Charlier

**Project Module Representative**

Gunnar Busch

**Project Manager**

Tim Munsie

**Past Project Manager**

Nikolaj Andresen

**Founding System Developer**

Shaun Newsome

And the other contributors to the system too numerous to name.
